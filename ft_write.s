%define MACH_SYSCALL(nb)    0x2000000 | nb  ; convertion to exa of syscall index
%define WRITE               4               ; syscall numbre of write
; %include <errno.h>ß
section .text
    extern  ___error
    global  _ft_write

_ft_write:
    push rbp                                ; ???
    mov rbp, rsp                            ; ???


    mov rax, MACH_SYSCALL(WRITE)            ; give the syscall number of write instruction
    syscall                                 ; execute the syscall
    jc error

    leave                                   ; pop registery pushed before
    ret

    error:
    mov rbx, rax
    call    ___error
    mov [rax], rbx
    mov rax, -1
    leave                                   ; pop registery pushed before
    ret