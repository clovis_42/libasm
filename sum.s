section .data
a:
    .value dq 2
b:
    .value dq 3

section .text
    global start
    global _main
    global _sum

start:
    call _main
    ret

_main:
    call _sum
    ret

_sum:
    push rbp
    mov rax, [rel a]
    mov rbx, [rel b]
    add rax, rbx
    mov rdi, 1
    lea rsi, rel rax
    mov rdx, 64
    mov rax, 0x2000004
    syscall
    leave
    ret