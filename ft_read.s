%define MACH_SYSCALL(nb)    0x2000000 | nb  ; convertion to exa of syscall index
%define READ               3               ; syscall numbre of read

section .text
    global _ft_read
    extern ___error

_ft_read:
    push rbp                                ; ???
    mov rbp, rsp                            ; ???


    mov rax, MACH_SYSCALL(READ)            ; give the syscall number of write instruction
    syscall                                 ; execute the syscall
    jc error

    leave                                   ; pop registery pushed before
    ret

    error:
    mov rbx, rax
    call   ___error
    mov [rax], rbx
    mov rax, -1
    leave                                   ; pop registery pushed before
    ret