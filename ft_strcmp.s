section .text
    global  _ft_strcmp

_ft_strcmp:
    push    rbp                                ; ???
    mov     rbp, rsp                           ; ???
    xor     rax, rax                           ; init incremtor 


    ; rdi   s1
    ; rsi   s2
loop:
    ; Is matched? 
    xor rcx, rcx
    xor rdx, rdx
    mov cl, [rel rdi + rax]
    mov dl, [rel rsi + rax]
    inc rax
    cmp dl, 0
    jz return
    cmp cl, 0
    jz return
    cmp dl, cl
    jz loop

    jmp return

return:
    sub rcx, rdx
    xor rax, rax
    mov rax, rcx
    leave
    ret