section .data
hello:
    .string db "Hello World  !", 10
    .len    equ $ - hello.string

section .text
    global start
    global _main
    global _helloworld

start:
    call _main
    ret

_main:
    call _helloworld
    ret

_helloworld:
    push rbp
    mov rbp, rsp
    sub rsp, 16
    
    mov rdi, 1
    lea rsi, [rel hello.string]
    mov rdx, rel hello.len
    mov rax, 0x2000004
    syscall
    cmp rax, 0
    jl error

    leave                                   ; pop registery pushed before
    ret

    error:
    neg rax
    mov rbx, rax
    call    _errno_location
    mov [rax], rbx
    mov rax, -1
    leave                                   ; pop registery pushed before
    ret