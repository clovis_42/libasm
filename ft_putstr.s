section .text
    global  _ft_putstr
    extern  _ft_putchar

_putstr:
    push    rbp                                ; ???
    mov     rbp, rsp                           ; ???

    mov     r12, rdi
    xor     r13, r13                           ; init incremtor
    push    r12


loop:
    ; Is end of string?
    cmp     BYTE [r12], 0
    jz      end

    mov     rdi, r12
    call    _ft_putchar

    inc     r12
    jmp     loop

end:
    pop     r12
    leave
    ret
    