section .text
    global  _ft_strchr

_ft_strchr:
    push    rbp                                ; ???
    mov     rbp, rsp                           ; ???
    xor     rax, rax                           ; init incremtor 

loop:
    ; Is end of string?
    cmp BYTE [rdi + rax], 0
    jz end

    ; Is matched? 
    mov dl, [rel rsi]
    mov cl, [rel rdi + rax]
    cmp dl, cl
    jz good

    inc rax
    jmp loop

good:
    add rax, rdi
    leave
    ret

end:
    xor   rax,  rax
    leave
    ret