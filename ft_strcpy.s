section .text
    global  _ft_strcpy

_ft_strcpy:
    push    rbp                                ; ???
    mov     rbp, rsp                           ; ???
    xor     rax, rax                           ; init incremtor 

    ; rdi dst
    ; rsi src
loop:
    mov     cl, [rel rsi + rax]
    mov     [rel rdi + rax], cl
    inc     rax

    ; Is end of string?
    cmp cl, 0
    jz end
    jmp loop

end:
    mov   [rel rdi + rax], BYTE 0
    mov   rax,  rdi
    leave
    ret