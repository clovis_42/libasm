section .text
    global  _ft_strdup
    extern  ___error
    extern  _ft_strcpy
    extern  _ft_strlen
    extern  _malloc
_ft_strdup:
    push rbp                                ; 
    mov rbp, rsp                            ; align stack
    ;strlen
    call    _ft_strlen
    ;malloc
    mov     rsi, rdi                        ;store first parameter in second parameter register(dst)
    mov     rdi, rax                        ;move result of strlen in parmeter 1 register
    push    rsi                             ;push first parameter to protect it from the malloc call
    sub     rsp, 8                          ;sub 8  to aline the stack to 16(base)+8 because fuction address(bytes) is store on stack
    add     rdi, 1                          ;add size for the \0 at the end of the string
    call    _malloc
    add     rsp, 8                          ;realline the stack after the function call
    pop     rsi                             ;retrive the rsi store on stack
    mov     rdi, rax                        ;put address return by malloc in first parameter register(dst)
    call    _ft_strcpy

    leave                                   ; pop registery pushed before
    ret