#ifndef     LIBASM_H
# define    LIBASM_H
# include   <errno.h>
unsigned long   ft_strlen(const char *str);
char*           ft_strcpy(char *dst, const char *src);
int             ft_strcmp(const char *s1, const char *s2);
unsigned long   ft_write(int fd, const void *buff,unsigned int  nbyte);
unsigned long   ft_read(int fd, void *buff, unsigned int nbyte);
char *          ft_strdup(const char *str);
#endif