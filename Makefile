NAME 	=	libasm.a
CCFLAGS	=	-f macho64
CC		=	/Users/clmenega/Bin/nasm
LK		=	ld
LKFLAGS	=	-arch x86_64 -lSystem
SRCS	= 	ft_putchar.s	\
			ft_strchr.s		\
			ft_strlen.s		\
			main.s			\
			ft_putstr.s		\
			ft_strcpy.s		\
			ft_strcmp.s		\
			ft_read.s 		\
			ft_write.s 		\
			ft_strdup.s 	\

OBJS 	= $(SRCS:%.s=%.o)

all:	$(NAME)

$(NAME)	: $(OBJS) Makefile
			ar rc $(NAME) $(OBJS)
			ranlib $(NAME)
			make clean


%.o 	: %.s libasm.h Makefile
		$(CC) $(CCFLAGS) $< -o $@

clean	:
		rm -f *.o

fclean	: clean
		rm -rf $(NAME)

re		: fclean all

test	:$(OBJS) Makefile
			$(LK) $(LKFLAGS) $(OBJS)
			@echo --run test--
			./a.out
			@echo --end test--
			rm a.out
			make clean

.SILENT: