%define MACH_SYSCALL(nb)    0x2000004 | nb  ; convertion to exa of syscall index
%define STDOUT              1               ; STDOUT file descriptor
%define WRITE               4               ; syscall numbre of write

section .text
    global _ft_putchar
    extern ___error

_ft_putchar:
    push rbp                                ; ???
    mov rbp, rsp                            ; ???

    mov rsi, rdi                            ; get the char in param
    mov rdi, STDOUT                         ; give STDOUT fd
    mov rdx, 1                              ; size of char 
    mov rax, MACH_SYSCALL(WRITE)            ; give the syscall number of write instruction
    syscall                                 ; execute the syscall
    cmp rax, 0
    jl error

    leave                                   ; pop registery pushed before
    ret

    error:
    neg rax
    mov rbx, rax
    call    ___error
    mov [rax], rbx
    mov rax, -1
    leave                                   ; pop registery pushed before
    ret
