section .data
    char:   db 'o'
    err:    db 'x'
    EOF:    db 'E'
    zero:   db '1'
    the:    db 'e'
    tha:    db 'e'
section .text
    global  _ft_strlen

_ft_strlen:
    push    rbp                                ; ???
    mov     rbp, rsp                           ; ???
    xor     rax, rax                           ; init incremtor 

loop:
    ; Is end of string?
    cmp BYTE [rdi + rax], 0
    jz return

    inc rax
    jmp loop

return:
    leave
    ret